# FizzBuzz Kata Template

## Introduction

### Iteration 1

Code an algorithm that accepts a numerical input and returns:
    
* `"FIZZ"` if the input value is divisible by three
* `"BUZZ"` if the input value is divisible by five
* `"FIZZBUZZ"` if the input value is divisible by three and five  
* The input value, if nothing else applies 

### More Iterations

Further challenges can be found in the `_doc` directory.
Please go through them step by step, and try not to read them all in advance to keep a fresh mind.

## Setup

This setup requires PHP `^7.3` if you do not have it installed locally, you can use docker instead.

### Docker Usage

To run composer install:

`$ docker run --rm --interactive --tty --volume $PWD:/app composer install`

To create an interactive docker container, run:

`$ docker run -it --rm --name fizz-buzz-kata -v "$PWD":/app -w /app php:7.3-cli bash`

Or to run the test suite:

`$ docker run --rm -v "$PWD":/app -w /app php:7.3-cli php ./vendor/bin/phpunit`


## Credit

See the original kata at [Coding Dojo](http://codingdojo.org/kata/FizzBuzz/)
